# The default profile for Apertis and derivatives thereof.
Profile: apertis/main
Extends: debian/main
Disable-Tags:
 initial-upload-closes-no-bugs
 bugs-field-does-not-refer-to-debian-infrastructure
 latest-changelog-entry-without-new-date
 bogus-mail-host-in-debian-changelog
 newer-standards-version
 pkg-js-tools-test-is-missing
 wrong-bug-number-in-closes
 improbable-bug-number-in-closes
 bogus-mail-host
 no-nmu-in-changelog
 package-has-long-file-name
 source-nmu-has-incorrect-version-number
 no-manual-page

Tags: dir-or-file-in-opt
Overridable: yes
