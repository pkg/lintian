This directory contains the lintian profile for the Apertis distribution and derivatives.
Below you can find some details on what the profile supports and the reasons behind it.
-------------------------------------------------------------------

initial-upload-closes-no-bugs
- Apertis doesn't use ITP bugs

bugs-field-does-not-refer-to-debian-infrastructure
- Apertis doesn't use the Debian infrastructure

latest-changelog-entry-without-new-date
- OBS generates malformed changelog entry without a new date (i.e. reusing same date as previous entry)

bogus-mail-host-in-debian-changelog
- OBS generates malformed changelog entry with invalid email

newer-standards-version
- It is common to have packages compliant to newer standards-version

pkg-js-tools-test-is-missing
- Tag with issue in v2021: This tag is triggered for all packages
  that use a variable in the dh invocation, like in:
     dh $@ --builddirectory=$(CURDIR)/_build
  Lintian assumes the variable $(CURDIR) contains the "--with nodejs" dh addon,
  but the package doesn't provide a testsuite in "debian/tests/pkg-js/test"
  that is executed by this nodejs addon. Thus, the tag is triggered.
  Refer to https://bugs.debian.org/928283 for more details

wrong-bug-number-in-closes
- Apertis derivatives use this field with bug numbers that do not
  follow the Debian numbering rules. For example:
     Closes: #BLABLA-1234

improbable-bug-number-in-closes
- Apertis derivatives use this field with bug numbers that do not
  refer to Debian bugs.

bogus-mail-host
- OBS generates malformed email address: "obs@buildd"

dir-or-file-in-opt
- Apertis derivatives use symlinks in /opt
  to avoid breaking legacy parts, thus could be useful
  to override this tag instead of completely removing it
  and leaving files forever in /opt

no-nmu-in-changelog
source-nmu-has-incorrect-version-number
- Apertis doesn't use non-maintainer upload, all packages are
  team maintained.

package-has-long-file-name
- OBS appends the commit hash to the package version which results in
  frequently triggering this tag. Moreover, this tag was created because
  packages with a very long filename cause issues when building CDs.
  This tag is unrelevant for Apertis, because Apertis is not shipped on CD.

no-manual-page
- Most of the binaries aren't provided with a manpage. Maintainers override it
  most of the time since manpages do not add more values for Apertis. So, let's
  disable it here to save time.
